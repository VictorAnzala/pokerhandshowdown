﻿namespace PokerHandShowdown.Hands
{
    public interface IDeck
    {
        IHand GetHand();
    }
}
