using System;
using System.Linq;
using System.Collections.Generic;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdown.Hands
{
    public class Deck : IDeck
    {
        private readonly Stack<ICard> _deck;
        private int playerNumber;

        public Deck ()
        {
            playerNumber = 1;
            var orderedDeck = new List<ICard>(Constants.Deck.DeckSize);

            for (int i = 0; i < orderedDeck.Capacity; i++)
            {
                if (i < Constants.Deck.SuitSize)
                {
                    orderedDeck.Add(new Diamond((CardValues) i));
                }
                else if (i < (Constants.Deck.SuitSize * 2))
                {
                    orderedDeck.Add(new Heart((CardValues) (i % Constants.Deck.SuitSize)));
                }
                else if (i < (Constants.Deck.SuitSize * 3))
                {
                    orderedDeck.Add(new Club((CardValues) (i % Constants.Deck.SuitSize)));
                }
                else
                {
                    orderedDeck.Add(new Spade((CardValues) (i % Constants.Deck.SuitSize)));
                }
            }

            _deck = Shuffle(orderedDeck, new Random());
        }

        //Modern Fisher-Yates
        private Stack<ICard> Shuffle(List<ICard> shuffledDeck, Random random)
        {
            for (int i = 0; i < shuffledDeck.Count - 1; i++)
            {
                Swap(shuffledDeck, i, random.Next(i, shuffledDeck.Count));
            }

            return new Stack<ICard>(shuffledDeck);
        }

        private void Swap(List<ICard> shuffledDeck, int i, int j)
        {
            var temp = shuffledDeck[i];
            shuffledDeck[i] = shuffledDeck[j];
            shuffledDeck[j] = temp;
        }

        public IHand GetHand()
        {
            List<ICard> newHand = new List<ICard>(5);

            for (int i = 0; i < Constants.Hand.HandSize && _deck.Any(); i++)
            {
                newHand.Add(_deck.Pop());
            }

            newHand = newHand.GroupBy(card => card.Value)
                .OrderBy(group => group.Count())
                .ThenBy(group => group.Key)
                .Reverse()
                .SelectMany(group => group)
                .ToList();            

            if(newHand.Count != Constants.Hand.HandSize) return null;            
            
            return new Hand(newHand, playerNumber++);            
        }
    }
}