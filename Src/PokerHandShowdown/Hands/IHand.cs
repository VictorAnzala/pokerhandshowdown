using System;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdown.Hands
{
    public interface IHand    
    {
        string GetPlayerName();
        void PrintHand();
        bool IsOnePair();
        bool IsTwoPair();
        bool IsThreeOfAKind();
        bool IsFourOfAKind();
        bool IsFlush();
        bool IsStraight();
        ICard PeekHighestCard();
        ICard PopHighestCard();
        int NumCards();
    }
}