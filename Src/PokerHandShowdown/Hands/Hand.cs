using System;
using System.Linq;
using System.Collections.Generic;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdown.Hands
{
    public class Hand : IHand
    {
        private readonly Stack<ICard> _stackedHand;
        private readonly List<ICard> _hand;
        private readonly int _playerNumber;
        private readonly string _playerName;

        public Hand (List<ICard> hand, int playerNumber)
        {
            _hand = hand;            
            _stackedHand = new Stack<ICard>((hand.Select(h => (ICard) h.Clone())).Reverse());                       
            _playerNumber = playerNumber;
            _playerName = $"Player {playerNumber}";
        }

        public string GetPlayerName()
        {
            return _playerName;
        }

        public void PrintHand()
        {
            Console.WriteLine($"Hand for {_playerName}:");
            Console.WriteLine(string.Format("{0}, {1}, {2}, {3}, {4}\n", _hand.ToArray()));
        }

        public bool IsFlush()
        {
            return _hand.All(card => card.Suit == CardSuits.Diamonds)
                || _hand.All(card => card.Suit == CardSuits.Hearts)
                || _hand.All(card => card.Suit == CardSuits.Clubs)
                || _hand.All(card => card.Suit == CardSuits.Spades);            
        }

        public bool IsStraight()
        {
            for (int i = 1; i < _hand.Count(); i++)
            {
                if(_hand[i - 1].Value != _hand[i].Value + 1) return false;
            }

            return true;
        }

        private bool OfAKindHelper(int kindNumber, int numGroups = 1)
        {            
            return _hand.GroupBy(card => card.Value)
                .Count(group => group.Count() == kindNumber) == numGroups;            
        }

        public bool IsOnePair()
        {
            return OfAKindHelper(2);
        }

        public bool IsTwoPair()
        {
            return OfAKindHelper(2, 2);
        }

        public bool IsThreeOfAKind()
        {
            return OfAKindHelper(3);
        }

        public bool IsFourOfAKind()
        {
            return OfAKindHelper(4);
        }

        public ICard PeekHighestCard()
        {
            if(_stackedHand.Any())
            {
                return _stackedHand.Peek();
            }
            else
            {
                return null;
            }
        }

        public ICard PopHighestCard()
        {
            if(_stackedHand.Any())
            {
                return _stackedHand.Pop();
            }
            else
            {
                return null;
            }
        }

        public int NumCards()
        {
            return _stackedHand.Count();
        }
    }
}