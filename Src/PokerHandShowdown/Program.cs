﻿using System;
using System.Text;
using Infrastructure.Messaging;
using PokerHandShowdown.Game;
using PokerHandShowdown.Hands;

namespace PokerHandShowdown
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            string welcomeString = "Welcome to Poker Hand Showdown!";
            string underline = Showdown.GetUnderline(welcomeString.Length);
            Console.WriteLine($"{welcomeString}\n{underline}\n");

            string requestForInputString = "How many games of Poker do you want to run? ";
            Console.Write(requestForInputString);
            string result = Console.ReadLine();            
            int numGames = LoopForInput(requestForInputString, result);

            requestForInputString = "How many hands do you want in each game? ";
            Console.Write(requestForInputString);
            result = Console.ReadLine();
            int numHands = LoopForInput(requestForInputString, result);

            Console.WriteLine($"{underline}\n");
            for (int i = 1; i <= numGames; i++)
            {
                Console.WriteLine($"Game #{i}");
                Showdown.StartGame(new Deck(), new ConsoleWriter(), numHands);
            }
        }

        private static int LoopForInput(string requestForInputString, string result)
        {
            int value;
            while (!int.TryParse(result, out value))
            {
                Console.Write(requestForInputString);
                result = Console.ReadLine();
            }

            return value;
        }
    }
}
