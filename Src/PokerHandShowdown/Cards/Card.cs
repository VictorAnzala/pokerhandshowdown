using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdown.Cards
{
    public abstract class Card : ICard
    {
        protected Card (CardSuits suit, CardValues value)
        {
            Suit = suit;
            Value = value;
        }

        public CardSuits Suit {get; private set;}

        public CardValues Value {get; private set;}

        public override string ToString()
        {
            return $"{Value} {Suit.GetDescription()}";
        }

        public abstract object Clone();
    }
}
