using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdown.Cards
{
    public class Heart : Card
    {
        public Heart (CardValues value) : base(CardSuits.Hearts, value) { }

        public override object Clone()
        {
            return new Heart(this.Value);
        }
    }
}

