using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdown.Cards
{
    public class Diamond : Card
    {
        public Diamond (CardValues value) : base(CardSuits.Diamonds, value) { }

        public override object Clone()
        {
            return new Diamond(this.Value);
        }
    }
}

