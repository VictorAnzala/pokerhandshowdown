using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdown.Cards
{
    public class Club : Card
    {
        public Club (CardValues value) : base(CardSuits.Clubs, value) { }

        public override object Clone()
        {
            return new Club(this.Value);
        }
    }
}
