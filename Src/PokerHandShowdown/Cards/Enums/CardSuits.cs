using System.ComponentModel;


namespace PokerHandShowdown.Cards.Enums
{
    public enum CardSuits
    {
        [Description("♦")]
        Diamonds,
        
        [Description("♣")]
        Clubs,
        
        [Description("♥")]
        Hearts,
        
        [Description("♠")]
        Spades
    }
}