using System;
using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdown.Cards
{
    public interface ICard : ICloneable
    {
        CardSuits Suit {get;}
        CardValues Value {get;}        
    }
}
