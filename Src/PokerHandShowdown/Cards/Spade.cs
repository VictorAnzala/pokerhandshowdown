using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdown.Cards
{
    public class Spade : Card
    {
        public Spade (CardValues value) : base(CardSuits.Spades, value) { }

        public override object Clone()
        {
            return new Spade(this.Value);
        }
    }
}