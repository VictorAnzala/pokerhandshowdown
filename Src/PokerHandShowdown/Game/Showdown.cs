using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure.Messaging;
using PokerHandShowdown.Hands;
using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdown.Game
{
    public static class Showdown
    {
        private static List<IHand> _hands = new List<IHand>();
        private static IConsoleWriter _writer;
        private static List<Func<ShowdownState>> _gameSequences = new List<Func<ShowdownState>>();  

        public static void StartGame(IDeck deck,
            IConsoleWriter writer,
            int numPlayers = Constants.Showdown.NumberOfPlayers)
        {
            _writer = writer;

            _hands.Clear();
            InitializeGameSequences();            

            for (int i = 0; i < numPlayers; i++)
            {
                _hands.Add(deck.GetHand());
                
                _hands.Last().PrintHand();
            }

            
            foreach (var sequence in _gameSequences)
            {
                switch(sequence.Invoke())
                {
                    case ShowdownState.Winner: 
                    case ShowdownState.Tie: return;
                }
            }            

            //When no player has any pair, highest card wins
            CheckHighCard(_hands);
        }

        private static void InitializeGameSequences()
        {
            _gameSequences.Clear();
            _gameSequences.Add(CheckStraightFlush);
            _gameSequences.Add(CheckFourOfAKind);
            _gameSequences.Add(CheckFullHouse);
            _gameSequences.Add(CheckFlushes);
            _gameSequences.Add(CheckStraights);            
            _gameSequences.Add(CheckThreeOfAKind);
            _gameSequences.Add(CheckTwoPair);
            _gameSequences.Add(CheckOnePair);            
        }    

        private static ShowdownState CheckStraightFlush()
        {
            var winningHands = _hands.Where(hand => hand.IsStraight()
                                                    && hand.IsFlush());

            bool isRoyalFlush = winningHands.Any(hand => hand.PeekHighestCard().Value == CardValues.Ace);

            return CheckHelper(winningHands, $" wins with a {(isRoyalFlush ? "Royal" : "Straight")} Flush!");
        }

        private static ShowdownState CheckFourOfAKind()
        {
            var winningHands = _hands.Where(hand => hand.IsFourOfAKind());

            return CheckHelper(winningHands, $" wins with a Four of a Kind!");
        }

        private static ShowdownState CheckFullHouse()
        {
            var winningHands = _hands.Where(hand => hand.IsThreeOfAKind()
                                                    && hand.IsOnePair());

            return CheckHelper(winningHands, $" wins with a Full House!");
        }

        private static ShowdownState CheckFlushes()
        {
            var winningHands = _hands.Where(hand => hand.IsFlush());

            return CheckHelper(winningHands, $" wins with a Flush!");            
        }        

        private static ShowdownState CheckStraights()
        {
            var winningHands = _hands.Where(hand => hand.IsStraight());

            return CheckHelper(winningHands, $" wins with a Straight!");
        }

        private static ShowdownState CheckThreeOfAKind()
        {
            var winningHands = _hands.Where(hand => hand.IsThreeOfAKind());

            return CheckHelper(winningHands, $" wins with a Three of a Kind!");
        }

        private static ShowdownState CheckTwoPair()
        {
            var winningHands = _hands.Where(hand => hand.IsTwoPair());

            return CheckHelper(winningHands, $" wins with a Two Pair!");
        }

        private static ShowdownState CheckOnePair()
        {
            var winningHands = _hands.Where(hand => hand.IsOnePair());

            return CheckHelper(winningHands, $" wins with a One Pair!");
        }


        private static ShowdownState CheckHighCard(List<IHand> hands, string winnerMessage = " wins with High Card!")
        {
            string underLine;            

            while (hands[0].PeekHighestCard() != null)
            {
                var groups = hands.Select(hand => hand.PeekHighestCard())
                    .GroupBy(card => card.Value)
                    .OrderBy(group => group.Key)
                    .Reverse();

                if (groups.First().Count() == 1)
                {
                    var winningHand = hands.First(h => h.PeekHighestCard().Value == groups.First().Key);
                    var winnerString = $"{winningHand.GetPlayerName()}{winnerMessage}";
                    underLine = GetUnderline(winnerString.Length);
                    _writer.WriteLine($"{winnerString}\n{underLine}\n");
                    return ShowdownState.Winner;
                }
                else
                {
                    var i = 0;
                    while (i < hands.Count())
                    {
                        if (hands[i].NumCards() != 1 &&
                            hands[i].PeekHighestCard().Value != groups.First().Key)
                        {
                            hands.RemoveAt(i);
                            continue;
                        }
                        hands[i].PopHighestCard();
                        i++;
                    }
                }
            }

            var drawCandidates = string.Join(" and ", hands.Select(h => h.GetPlayerName()).ToArray());
            var drawString = $"It's a draw between {drawCandidates}!";
            underLine = GetUnderline(drawString.Length);
            _writer.WriteLine($"{drawString}\n{underLine}\n");
            return ShowdownState.Tie;
        }

        private static ShowdownState CheckHelper(IEnumerable<IHand> winningHands, string winnerMessage)
        {
            switch (winningHands.Count())
            {
                case 0: return ShowdownState.Continue;

                case 1: 
                {
                    var winnerString = $"{winningHands.First().GetPlayerName()}{winnerMessage}";
                    var underLine = GetUnderline(winnerString.Length); 
                    _writer.WriteLine($"{winnerString}\n{underLine}\n");
                    return ShowdownState.Winner;
                }

                default: return CheckHighCard(winningHands.ToList(), winnerMessage);
            }
        }

        public static string GetUnderline(int length, char lineCharacter = '-')
        {
            return new string (lineCharacter, length);
        }
}
}