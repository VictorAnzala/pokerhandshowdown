namespace PokerHandShowdown.Game
{
    public enum ShowdownState
    {
        Winner,
        Tie,
        Continue
    }
}