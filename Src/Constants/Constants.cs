namespace Constants
{
    public static class Deck
    {
        public const int DeckSize = 52;
        public const int SuitSize = 13;
    }               

    public static class Hand
    {
        public const int HandSize = 5;
    }

    public static class Showdown
    {
        public const int NumberOfPlayers = 4;
    }
}