﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandShowdown.Cards.Enums;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Hands;
using System.Collections.Generic;
using PokerHandShowdown.Game;

namespace PokerHandShowdownTests.ShowdownTests
{
    [TestClass]
    public class When_player_two_has_straight_flush : ShowdownTest
    {
        protected override void GivenThat()
        {
            TestHands = new List<IHand>
            {
                new Hand(new List<ICard>
                {
                    new Club(CardValues.Ten),
                    new Club(CardValues.Eight),
                    new Club(CardValues.Six),
                    new Club(CardValues.Four),
                    new Club(CardValues.Two)
                },1),
                new Hand(new List<ICard>
                {
                    new Diamond(CardValues.Eight),
                    new Diamond(CardValues.Seven),
                    new Diamond(CardValues.Six),
                    new Diamond(CardValues.Five),
                    new Diamond(CardValues.Four)
                },2),
                new Hand(new List<ICard>
                {
                    new Heart(CardValues.Six),
                    new Heart(CardValues.Five),
                    new Heart(CardValues.Four),
                    new Heart(CardValues.Three),
                    new Heart(CardValues.Two)
                },3),
            };

            base.GivenThat();
        }

        [TestMethod]
        public void Should_give_win_to_player_two()
        {
            GivenThat();

            Showdown.StartGame(MockedDeck.Object, MockedConsoleWriter.Object, 3);

            var winningString = "Player 2 wins with a Straight Flush!";
            var underline = Showdown.GetUnderline(winningString.Length);
            MockedConsoleWriter
                .Verify(dep => dep.WriteLine($"{winningString}\n{underline}\n"));
        }
    }
}
