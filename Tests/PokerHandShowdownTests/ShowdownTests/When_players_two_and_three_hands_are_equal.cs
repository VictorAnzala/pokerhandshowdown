﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandShowdown.Cards.Enums;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Hands;
using System.Collections.Generic;
using PokerHandShowdown.Game;

namespace PokerHandShowdownTests.ShowdownTests
{
    [TestClass]
    public class When_players_two_and_three_hands_are_equal : ShowdownTest
    {
        protected override void GivenThat()
        {
            TestHands = new List<IHand>
            {
                new Hand(new List<ICard>
                {
                    new Club(CardValues.Ten),
                    new Club(CardValues.Eight),
                    new Club(CardValues.Six),
                    new Club(CardValues.Four),
                    new Club(CardValues.Two)
                },1),
                new Hand(new List<ICard>
                {
                    new Diamond(CardValues.Six),
                    new Diamond(CardValues.Five),
                    new Diamond(CardValues.Four),
                    new Diamond(CardValues.Three),
                    new Diamond(CardValues.Two)
                },2),
                new Hand(new List<ICard>
                {
                    new Heart(CardValues.Six),
                    new Heart(CardValues.Five),
                    new Heart(CardValues.Four),
                    new Heart(CardValues.Three),
                    new Heart(CardValues.Two)
                },3),
            };

            base.GivenThat();
        }

        [TestMethod]
        public void Should_give_draw()
        {
            GivenThat();

            Showdown.StartGame(MockedDeck.Object, MockedConsoleWriter.Object, 3);

            var drawString = "It's a draw between Player 2 and Player 3!";
            var underline = Showdown.GetUnderline(drawString.Length);
            MockedConsoleWriter
                .Verify(dep => dep.WriteLine($"{drawString}\n{underline}\n"));
        }
    }
}
