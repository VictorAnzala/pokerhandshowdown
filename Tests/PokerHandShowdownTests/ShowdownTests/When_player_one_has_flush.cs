﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandShowdown.Cards.Enums;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Hands;
using System.Collections.Generic;
using PokerHandShowdown.Game;

namespace PokerHandShowdownTests.ShowdownTests
{
    [TestClass]
    public class When_player_one_has_flush : ShowdownTest
    {
        protected override void GivenThat()
        {
            TestHands = new List<IHand>
            {
                new Hand(new List<ICard>
                {
                    new Club(CardValues.Ten),
                    new Club(CardValues.Eight),
                    new Club(CardValues.Six),
                    new Club(CardValues.Four),
                    new Club(CardValues.Two)
                },1),
                new Hand(new List<ICard>
                {
                    new Spade(CardValues.Four),
                    new Diamond(CardValues.Four),
                    new Diamond(CardValues.Nine),
                    new Diamond(CardValues.Seven),
                    new Club(CardValues.Five),                    
                },2),
                new Hand(new List<ICard>
                {
                    new Heart(CardValues.Seven),
                    new Heart(CardValues.Five),
                    new Heart(CardValues.Four),
                    new Heart(CardValues.Three),
                    new Heart(CardValues.Two)
                },3),
            };

            base.GivenThat();
        }

        [TestMethod]
        public void Should_give_win_to_player_one()
        {
            GivenThat();

            Showdown.StartGame(MockedDeck.Object, MockedConsoleWriter.Object, 3);

            var winningString = "Player 1 wins with a Flush!";
            var underline = Showdown.GetUnderline(winningString.Length);
            MockedConsoleWriter
                .Verify(dep => dep.WriteLine($"{winningString}\n{underline}\n"));
        }
    }
}
