﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandShowdown.Cards.Enums;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Hands;
using System.Collections.Generic;
using PokerHandShowdown.Game;

namespace PokerHandShowdownTests.ShowdownTests
{
    [TestClass]
    public class When_player_two_has_one_pair : ShowdownTest
    {
        protected override void GivenThat()
        {
            TestHands = new List<IHand>
            {
                new Hand(new List<ICard>
                {
                    new Club(CardValues.Ten),
                    new Spade(CardValues.Eight),
                    new Club(CardValues.Six),
                    new Spade(CardValues.Four),
                    new Club(CardValues.Two)
                },1),
                new Hand(new List<ICard>
                {
                    new Diamond(CardValues.Nine),
                    new Heart(CardValues.Nine),
                    new Club(CardValues.King),
                    new Spade(CardValues.Five),
                    new Diamond(CardValues.Four)
                },2),
                new Hand(new List<ICard>
                {
                    new Spade(CardValues.Seven),
                    new Heart(CardValues.Five),
                    new Heart(CardValues.Four),
                    new Diamond(CardValues.Three),
                    new Heart(CardValues.Two)
                },3),
            };

            base.GivenThat();
        }

        [TestMethod]
        public void Should_give_win_to_player_two()
        {
            GivenThat();

            Showdown.StartGame(MockedDeck.Object, MockedConsoleWriter.Object, 3);

            var winningString = "Player 2 wins with a One Pair!";
            var underline = Showdown.GetUnderline(winningString.Length);
            MockedConsoleWriter
                .Verify(dep => dep.WriteLine($"{winningString}\n{underline}\n"));
        }
    }
}
