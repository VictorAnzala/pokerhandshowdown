﻿using Moq;
using PokerHandShowdown.Hands;
using System.Collections.Generic;
using Infrastructure.Messaging;

namespace PokerHandShowdownTests.ShowdownTests
{
    public abstract class ShowdownTest
    {        
        protected List<IHand> TestHands;
        protected Mock<IDeck> MockedDeck;
        protected Mock<IConsoleWriter> MockedConsoleWriter;
        
        protected virtual void GivenThat()
        {
            MockedDeck = new Mock<IDeck>();

            MockedDeck
                .SetupSequence(dep => dep.GetHand())
                .Returns(TestHands[0])
                .Returns(TestHands[1])
                .Returns(TestHands[2]);

            MockedConsoleWriter = new Mock<IConsoleWriter>();
        }
    }
}
