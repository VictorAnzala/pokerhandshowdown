﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PokerHandShowdownTests.DeckTests
{
    [TestClass]
    public class When_deck_has_ran_out_of_cards : DeckTest
    {
        [TestMethod]
        public void Should_return_null_when_requesting_hand()
        {
            for (int i = 0; i < 10; i++)
            {
                Assert.IsNotNull(GetHand());
            }

            Assert.IsNull(GetHand());
        }
    }
}
