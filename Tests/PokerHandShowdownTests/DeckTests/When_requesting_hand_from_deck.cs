﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandShowdown.Hands;

namespace PokerHandShowdownTests.DeckTests
{
    [TestClass]
    public class When_requesting_hand_from_deck : DeckTest
    {
        [TestMethod]
        public void Should_get_hand_with_five_cards()
        {
            Assert.AreEqual(5, Constants.Hand.HandSize);

            IHand testHand = GetHand();

            for (int i = 0; i < Constants.Hand.HandSize; i++)
            {
                Assert.IsNotNull(testHand.PopHighestCard());
            }
        }
    }
}
