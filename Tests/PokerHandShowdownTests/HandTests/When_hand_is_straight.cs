﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdownTests.HandTests
{
    [TestClass]
    public class When_hand_is_straight : HandTest
    {
        private static List<ICard> _testHand = new List<ICard>
        {
            new Club(CardValues.Ten),
            new Diamond(CardValues.Nine),
            new Spade(CardValues.Eight),
            new Heart(CardValues.Seven),
            new Club(CardValues.Six)
        };

        public When_hand_is_straight() : base(_testHand, 1)
        { }

        [TestMethod]
        public void Should_return_true_for_is_straight()
        {
            Assert.IsTrue(IsStraight());
        }
    }
}
