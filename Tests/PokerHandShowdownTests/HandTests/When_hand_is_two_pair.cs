﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdownTests.HandTests
{
    [TestClass]
    public class When_hand_is_two_pair : HandTest
    {
        private static List<ICard> _testHand = new List<ICard>
        {
            new Club(CardValues.Seven),
            new Spade(CardValues.Seven),
            new Diamond(CardValues.Four),
            new Heart(CardValues.Four),
            new Spade(CardValues.Eight),
        };

        public When_hand_is_two_pair() : base(_testHand, 1)
        { }

        [TestMethod]
        public void Should_return_true_for_is_two_pair()
        {
            Assert.IsTrue(IsTwoPair());
        }
    }
}
