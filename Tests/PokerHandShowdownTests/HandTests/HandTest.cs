﻿using System.Collections.Generic;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Hands;

namespace PokerHandShowdownTests.HandTests
{
    public abstract class HandTest : Hand
    {
        public HandTest(List<ICard> hand, int playerNumber) : base(hand, playerNumber)
        { }
    }
}
