﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdownTests.HandTests
{
    [TestClass]
    public class When_hand_is_nothing : HandTest
    {
        private static List<ICard> _testHand = new List<ICard>
        {
            new Club(CardValues.Ten),
            new Diamond(CardValues.Eight),
            new Spade(CardValues.Six),
            new Heart(CardValues.Four),
            new Club(CardValues.Two)
        };

        public When_hand_is_nothing() : base(_testHand, 1)
        { }

        [TestMethod]
        public void Should_return_false_for_all()
        {
            Assert.IsFalse(IsFlush());
            Assert.IsFalse(IsStraight());
            Assert.IsFalse(IsOnePair());
            Assert.IsFalse(IsTwoPair());
            Assert.IsFalse(IsThreeOfAKind());
            Assert.IsFalse(IsFourOfAKind());
        }
    }
}
