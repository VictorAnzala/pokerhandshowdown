﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdownTests.HandTests
{
    [TestClass]
    public class When_hand_is_one_pair : HandTest
    {
        private static List<ICard> _testHand = new List<ICard>
        {
            new Club(CardValues.Six),
            new Spade(CardValues.Six),
            new Diamond(CardValues.Ace),
            new Heart(CardValues.Ten),            
            new Spade(CardValues.Eight),                        
        };

        public When_hand_is_one_pair() : base(_testHand, 1)
        { }

        [TestMethod]
        public void Should_return_true_for_is_one_pair()
        {
            Assert.IsTrue(IsOnePair());
        }
    }
}
