﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdownTests.HandTests
{
    [TestClass]
    public class When_hand_is_four_of_a_kind : HandTest
    {
        private static List<ICard> _testHand = new List<ICard>
        {
            new Club(CardValues.Jack),
            new Spade(CardValues.Jack),
            new Diamond(CardValues.Jack),
            new Heart(CardValues.Jack),
            new Spade(CardValues.Eight),
        };

        public When_hand_is_four_of_a_kind() : base(_testHand, 1)
        { }

        [TestMethod]
        public void Should_return_true_for_is_four_of_a_kind()
        {
            Assert.IsTrue(IsFourOfAKind());
        }
    }
}
