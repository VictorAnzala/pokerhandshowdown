﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandShowdown.Cards;
using PokerHandShowdown.Cards.Enums;

namespace PokerHandShowdownTests.HandTests
{
    [TestClass]
    public class When_hand_is_flush : HandTest
    {
        private static List<ICard> _testHand = new List<ICard>
        {
            new Club(CardValues.Ten),
            new Club(CardValues.Eight),
            new Club(CardValues.Six),
            new Club(CardValues.Four),
            new Club(CardValues.Two)
        };

        public When_hand_is_flush() : base(_testHand, 1)
        { }

        [TestMethod]
        public void Should_return_true_for_is_flush()
        {
            Assert.IsTrue(IsFlush());
        }
    }
}
